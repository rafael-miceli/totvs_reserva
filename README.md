# Descrição do projeto

Projeto para organizar reservas fictícias __Hard Coded__

## Como rodar o projeto?

Para rodar o projeto basta executar na ordem:

`dotnet restore`
`npm install`
`bower install`
`dotnet run`

> (Poderia depois melhorar colocando tudo em um só "restore")

## Especificação High Lvl:

O projeto é preparado para suportar diferentes ambientes pela pasta config, além de usar o gulp para automatizar diversar tasks.

Ao executar um `gulp web:build` o projeto tranfere o que esta na pasta `src` para a `wwwroot` com as injeções necessárias.

## Know Issues =/

- O layout não esta aplicado
- A escolha de mês e ano estão hard coded e não em um DatePicker
- A forma de drag n drop de reservas não está como pedido no enunciado
- Só é possível arrastar as reservas em sua data de início (na cor laranja)

## Melhorias =|

- Melhorar o drag n drop
- Deixar mais clean a lógica da aplicação

## Como rodar os testes?

Basta executar o comando `gulp test:unit`

