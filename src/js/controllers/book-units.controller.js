reservaApp.controller('BookUnitsController', ['$scope', 'UnitsService', 'DaysOfMonthService', function($scope, UnitsService, DaysOfMonthService) {
    
    $scope.units = [];
    $scope.booked_days_from_units = [];
    $scope.selected_year = 2017;
    $scope.selected_month = 3;

    //---- Exposed Methods 
    $scope.GetBookedUnitsFromPeriod = getBookedUnitsFromPeriod;
    $scope.GetDaysFromMonth = getDaysFromMonth;    
    $scope.MountBookedDaysFromEachUnit = mountBookedDaysFromEachUnit;
    $scope.OnSelectItem = onSelectItem;
    $scope.GetSelectedItemsIncluding = getSelectedItemsIncluding;
    $scope.OnDrop = onDrop;
    $scope.OnDragstart = onDragstart;
    $scope.OnMoved = onMoved;
    $scope.IsForbiddenDayToMove = isForbiddenDayToMove;
    $scope.SelectBook = selectBook;


    $scope.GetDaysFromMonth();
    $scope.GetBookedUnitsFromPeriod();        


    //---- Methods Implementations
    function getBookedUnitsFromPeriod() {
        $scope.is_loading = true;

        UnitsService.getBookedUnitsFromPeriod()
        .then(function(result){
            $scope.units = result.units;
            $scope.booked_days_from_units = result.unitsBookedInPeriod;

            $scope.MountBookedDaysFromEachUnit();

            $scope.is_loading = false;
        });        
    }

    function getDaysFromMonth() {
        $scope.days = DaysOfMonthService.getDaysFromMonth($scope.selected_year, $scope.selected_month);        
    }

    function mountBookedDaysFromEachUnit() {

        $scope.units_bookeds_days_map = [
            {
                unit_id: 0,
                unit: "",
                unit_type_id: 0,
                days: [
                    {num: '', book_moorning: false, book_afternoon: false}
                ]
            }
        ];

        $scope.units.forEach(function(unit) {
            var unit_bookeds_days_map = {unit: "", days: []};
            unit_bookeds_days_map.unit = unit.room;            
            unit_bookeds_days_map.unit_id = unit.id;            
            unit_bookeds_days_map.unit_type_id = unit.type_id;            

            var bookings_in_unit = [];

            $scope.booked_days_from_units.forEach(function(booked_period_from_unit) {
                if (booked_period_from_unit.unit_id == unit.id)
                    return bookings_in_unit.push(booked_period_from_unit);
            });                

            unit_bookeds_days_map.days = $scope.days.map(function(day) {                

                var new_day = {num: day, book_moorning: false, book_afternoon: false};

                bookings_in_unit.forEach(function(booking) {                    
                    if (booking.begin.toString() == day.toString()) 
                        new_day.book_afternoon = true;
                    if (booking.end.toString() == day.toString()) 
                        new_day.book_moorning = true;

                    if (day > booking.begin && day < booking.end) {
                        new_day.book_afternoon = true;
                        new_day.book_moorning = true;
                    }                        
                });                

                return new_day;
            });           

            $scope.units_bookeds_days_map.push(unit_bookeds_days_map);
        });
        
    }

    function onSelectItem(list, item, index) {       
        if (isForbiddenDayToMove(item))
            return;                
        selectBook(list, item, index);
    }

    function getSelectedItemsIncluding(list, item, index) {
        selectBook(list, item, index)
        return list.days.filter(function(item) { return item.selected; });
    };

    function onDrop(list, items, index) {        

        var last_booking_day = items.length - 1;

        //Verificar se pode mudar reserva
        if (list.days[index].book_afternoon || list.days[index + (last_booking_day)].book_afternoon){
            console.log("não pude mudar");
            return false;
        }        
        
        var days_changing_num = []; 
        for(var i = index; i < (index + last_booking_day + 1); i++) {
            days_changing_num.push(list.days[i].num);
        }

        items.forEach(function(item, item_index) { item.selected = false; item.num = days_changing_num[item_index]; });

        list.days = list.days.slice(0, index)
                    .concat(items)
                    .concat(list.days.slice(index + (last_booking_day + 1)));

        //Atualizar resumo de unidades vagas
        $scope.updateVacancy({items: {days: items, unit_type_id: list.unit_type_id}});
        
        return true;
    }

    function onDragstart(list, event) {
        list.dragging = true;
    };

    function onMoved(list) {        
        list.days = list.days.map(function(day) { 
            if(day.selected) {
                day.book_moorning = false;
                day.book_afternoon = false;                
                day.selected = false;
            }

            return day; 
        });
    };

    function isForbiddenDayToMove(item) {
        return (!item.book_moorning && !item.book_afternoon) 
        || (item.book_moorning && !item.book_afternoon) || (item.book_moorning && item.book_afternoon);
    };

    function selectBook(list, item, index) {
        item.selected = !item.selected;
        
        //Seleciona até o dia fim da reserva
        index_book_next_day = ++index;
        book_next_day = list.days[index_book_next_day];

        while(book_next_day.book_moorning && book_next_day.book_afternoon) {
            list.days[index_book_next_day].selected = !list.days[index_book_next_day].selected;
            index_book_next_day++;
            book_next_day = list.days[index_book_next_day];
        }

        list.days[index_book_next_day].selected = !list.days[index_book_next_day].selected;
    };
}]);