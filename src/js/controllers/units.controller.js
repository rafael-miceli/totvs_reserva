reservaApp.controller('UnitsController', ['$scope', 'UnitsService', 'DaysOfMonthService', function($scope, UnitsService, DaysOfMonthService) {
    $scope.units_type = [];    
    $scope.is_loading;
    $scope.selected_year = 2017;
    $scope.selected_month = 3;

    
    var unit_type = {        
        id: 0,
        name: '',
        days: [{
            day: '',
            vacancy_quantities: 0
        }]
    };

    
    $scope.GetVancatUnitsByTypeFromPeriod = getVancatUnitsByTypeFromPeriod;
    $scope.GetDaysFromMonth = getDaysFromMonth;
    $scope.UpdateVacancyComponent = updateVacancyComponent;


    $scope.GetDaysFromMonth();
    $scope.GetVancatUnitsByTypeFromPeriod();    

    


    function updateVacancyComponent(item){
        $scope.units_type.forEach(function(unit){
            if (unit.id == item.unit_type_id) {
                for(var i = 0; i < unit.days.length; i++) {
                    if(unit.days[i].day.toString() == item.days[0].num.toString()) {
                        for (var j = i; j < item.days.length; j++) {
                            unit.days.splice(j, 1);
                        }
                    }
                }
            }
        });
        $scope.GetDaysFromMonth();
    }

    function getVancatUnitsByTypeFromPeriod() {
        $scope.is_loading = true;
        UnitsService.getVacantUnitsFromPeriod()
        .then(function(result){
            
            var type_index = -1;
            var day_index = -1;
            var old_unit_type_id = 0;
            var old_date = '01/01/1977';
            var vacancy_quantities = 0;
            result.unitsVacantsInPeriod.forEach(function(unitVacantInPeriod){
                var new_unit_type_id = unitVacantInPeriod.type;                
                var new_date = unitVacantInPeriod.date;

                if (new_unit_type_id != old_unit_type_id) {
                    old_unit_type_id = new_unit_type_id;                   
                    vacancy_quantities = 1;
                    day_index = -1;

                    $scope.units_type.push({
                        id: old_unit_type_id,  
                        days: []                      
                    });

                    type_index++;
                    
                    old_date = new_date;
                    vacancy_quantities = 1;

                    $scope.units_type[type_index].days.push({ 
                        day: new_date,
                        vacancy_quantities: vacancy_quantities
                    });  

                    day_index++;                                     
                }                                

                if (new_date != old_date) {
                    old_date = new_date;
                    vacancy_quantities = 1;

                    $scope.units_type[type_index].days.push({ 
                        day: new_date,
                        vacancy_quantities: vacancy_quantities
                    });  

                    day_index++;                 
                }

                $scope.units_type[type_index].days[day_index].vacancy_quantities = vacancy_quantities++;
            });

            result.unitsType.forEach(function(unittype){
                $scope.units_type.forEach(function(unit_type){
                    if (unit_type.id == unittype.id)
                        unit_type.name = unittype.name;
                });
            });

            $scope.is_loading = false;
        });            
    }

    function getDaysFromMonth() {
        $scope.days = DaysOfMonthService.getDaysFromMonth($scope.selected_year, $scope.selected_month);        
    }
}]);