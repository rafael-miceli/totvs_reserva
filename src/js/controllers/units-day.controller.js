reservaApp.controller('VacancyUnitsDayController', ['$scope', function($scope) {    
    $scope.units_type = [];
    $scope.total_vacancy = 0;
    $scope.day_name = moment($scope.day, "D_M_YYYY").locale('pt-br').format('ddd')

    var unit_type = {
        id: 0,
        name: '',
        vacancy_quantities: 0
    };

    $scope.GetVancatUnitsByTypeFromDay = getVancatUnitsByTypeFromDay;

    $scope.GetVancatUnitsByTypeFromDay();    

    function getVancatUnitsByTypeFromDay() {
        $scope.is_loading = true;

        var index = -1;
        $scope.unitstype.forEach(function(unittype){
            $scope.units_type.push({
                id: unittype.id,
                name: unittype.name,
                vacancy_quantities: 0
            });

            index++;

            unittype.days.forEach(function(day_obj){                
                
                if (day_obj.day.toString() == $scope.day.toString()) {                    

                    $scope.units_type[index].vacancy_quantities += day_obj.vacancy_quantities;
                    $scope.total_vacancy += day_obj.vacancy_quantities;
                    
                    $scope.is_loading = false;
                    return;                    
                }
            });            
        });        
    }
}]);