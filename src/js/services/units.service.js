reservaAppServices
.factory('UnitsService', ['$http', '$q', 'appConfig', function($http, $q, appConfig) {     

    function getVacantUnitsFromPeriod() {

        //Unidades vagas de Abril

        var deferred = $q.defer();
        var data = {            
        "unitsType": [
            {
                "id": 1,
                "name": "ADP"
            },
            {
                "id": 2,
                "name": "SPD"
            }
        ],

        "unitsVacantsInPeriod": [
            {
                "type": 1,
                "room": "101",
                "date": new Date(2017, 3, 1)
            },
            {
                "type": 1,
                "room": "201",
                "date": new Date(2017, 3, 1)
            },
            {
                "type": 2,
                "room": "102",
                "date": new Date(2017, 3, 1)
            },
            {
                "type": 2,
                "room": "102",
                "date": new Date(2017, 3, 2)
            },
            {
                "type": 2,
                "room": "102",
                "date": new Date(2017, 3, 4)
            },
            {
                "type": 2,
                "room": "202",
                "date": new Date(2017, 3, 4)
            }
            
        ]};

        deferred.resolve(data);

        return deferred.promise;
    }     

    function getBookedUnitsFromPeriod() {

        //Unidades ocupadas de Abril

        var deferred = $q.defer();
        var data = {            
        "unitsType": [
            {
                "id": 1,
                "name": "ADP"
            },
            {
                "id": 2,
                "name": "SPD"
            }
        ],

        "units": [
            {
                "id": 1,
                "type_id": 1,
                "room": "101"
            },
            {
                "id": 2,
                "type_id": 1,
                "room": "201"
            },
            {
                "id": 3,
                "type_id": 2,
                "room": "102"
            },
            {
                "id": 4,
                "type_id": 2,
                "room": "202"
            },
            {
                "id": 5,
                "type_id": 2,
                "room": "302"
            }
        ],

        "unitsBookedInPeriod": [
            {                
                "unit_id": 1,
                "begin": new Date(2017, 3, 5),
                "end": new Date(2017, 3, 6)
            },
            {
                "unit_id": 2,
                "begin": new Date(2017, 3, 5),
                "end": new Date(2017, 3, 8)
            },
            {
                "unit_id": 3,
                "begin": new Date(2017, 3, 6),
                "end": new Date(2017, 3, 7)
            },
            {
                "unit_id": 3,
                "begin": new Date(2017, 3, 5),
                "end": new Date(2017, 3, 6)
            },
            {
                "unit_id": 3,
                "begin": new Date(2017, 3, 8),
                "end": new Date(2017, 3, 9)
            },
            {
                "unit_id": 4,
                "begin": new Date(2017, 3, 5),
                "end": new Date(2017, 3, 7)
            }
            
        ]};

        deferred.resolve(data);

        return deferred.promise;
    }     
    
    return {
        getVacantUnitsFromPeriod: getVacantUnitsFromPeriod,
        getBookedUnitsFromPeriod: getBookedUnitsFromPeriod
    }
}]);