reservaAppServices
.factory('DaysOfMonthService', [function() {

    function getDaysFromMonth(selected_year, selected_month) {

        var end_date = new Date(selected_year, selected_month + 1, 0);
        var days = [];
        for (var d = new Date(selected_year, selected_month, 1); d <= end_date; d.setDate(d.getDate() + 1)) {
            days.push(new Date(d));
        }

        return days;    
    }

    return {
        getDaysFromMonth: getDaysFromMonth
    }
}]);     