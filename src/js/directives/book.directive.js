reservaAppDirectives.directive ('bookcomponent', function() {
    return {
        templateUrl: './templates/book-component.html',
        controller: 'BookController',
        restrict: 'E'        
    }
});