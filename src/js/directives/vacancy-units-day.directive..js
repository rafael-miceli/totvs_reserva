reservaAppDirectives.directive ('vancancyunitsday', function($compile) {
    return {
        templateUrl: './templates/vancancy-units-day-component.html',
        controller: 'VacancyUnitsDayController',
        restrict: 'E',
        scope: {
            day: '=',
            unitstype: '='            
        }
    }
});