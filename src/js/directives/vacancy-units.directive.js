reservaAppDirectives.directive ('vancancyunits', function() {
    return {
        templateUrl: './templates/vancancy-units-component.html',
        controller: 'UnitsController',
        restrict: 'E',
        transclude: false
    }
});