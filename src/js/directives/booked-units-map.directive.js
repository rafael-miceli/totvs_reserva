reservaAppDirectives.directive ('bookedunits', function() {
    return {
        templateUrl: './templates/booked-units-map-component.html',
        controller: 'BookUnitsController',
        restrict: 'E',
        scope: {
            updateVacancy: '&'   
        }        
    }
});