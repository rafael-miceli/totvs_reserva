var reservaAppServices  = angular.module('reservaApp.services', []);
var reservaAppDirectives  = angular.module('reservaApp.directives', []);

var reservaApp = angular.module(
  'reservaApp', [
    'ngAnimate',
    'reservaApp.services',
    'reservaApp.directives',
    'dndLists'
  ]
);

reservaApp.constant('appConfig', {
  backendURL: '@@backendURL',
  env: '@@env'
});
