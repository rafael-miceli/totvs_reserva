describe('Book Units Controller', function() {
    var $controller, $scope, $rootScope; 

    beforeEach(module('reservaApp'));

    beforeEach(inject(function(_$controller_, _$rootScope_) {
        $controller = _$controller_;
		$scope = {};		
        $rootScope = _$rootScope_;
        $controller('BookUnitsController', { $scope: $scope });
    }));
    

    it('should load booked units from period', function() {       
        $rootScope.$apply();

        expect($scope.units_bookeds_days_map.length).toBe(6);      
    });

    it('should hide loading after load vacanties units', function() {       
        var expected_loading = false;    
        
        $rootScope.$apply();

        expect($scope.is_loading).toBe(expected_loading);      
    });

    it('should load the days from selected month and year', function() {               
        $rootScope.$apply();

        $scope.selected_year = 2017;
        $scope.selected_month = 4;

        $scope.GetDaysFromMonth();

        expect($scope.days.length).toBe(31);      
    });

    it('should be forbidden to move book from all day', function() {               
        $rootScope.$apply();

        var result = $scope.IsForbiddenDayToMove({book_moorning: true, book_afternoon: true});

        expect(result).toBe(true);      
    });

    it('should be forbidden to move no book in the day', function() {               
        $rootScope.$apply();

        var result = $scope.IsForbiddenDayToMove({book_moorning: false, book_afternoon: false});

        expect(result).toBe(true);      
    });

    it('should get today and next day in one "day" book', function() {               
        $rootScope.$apply();

        var list = {
            days: [
                {
                    num: new Date(2017, 3, 2),
                    book_moorning: false, 
                    book_afternoon: true
                },
                {
                    num: new Date(2017, 3, 3),
                    book_moorning: true, 
                    book_afternoon: false
                }
            ]
        }

        var index = 0;

        var result = $scope.SelectBook(list, list.days[0], index);

        expect(list.days[0].selected).toBe(true);      
        expect(list.days[1].selected).toBe(true);      
    });
});

