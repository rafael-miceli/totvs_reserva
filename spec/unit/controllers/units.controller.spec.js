describe('App common', function() {
  var appConfig;

  beforeEach(module('reservaApp'));

  beforeEach(inject(function(_appConfig_) {
    appConfig = _appConfig_;
  }));

  describe('configuration', function () {
    it('should get the value of backendURL', function () {
      expect(appConfig.backendURL).toEqual('@@backendURL');
    });

    it('should set env', function () {
      expect(appConfig.env).toEqual('@@env');
    });
  });
});

describe('Units Controller', function() {
    var $controller, $scope, $rootScope; 

    beforeEach(module('reservaApp'));

    beforeEach(inject(function(_$controller_, _$rootScope_) {
        $controller = _$controller_;
		$scope = {};		
        $rootScope = _$rootScope_;
        $controller('UnitsController', { $scope: $scope });
    }));
    

    it('should load vacanties units', function() {       
        $scope.selected_year = 2017;
        $scope.selected_month = 3;

        $rootScope.$apply();

        expect($scope.units_type.length).toBe(2);      
    });

    it('should hide loading after load vacanties units', function() {       
        var expected_loading = false;    
        
        $rootScope.$apply();

        expect($scope.is_loading).toBe(expected_loading);      
    });

    it('should load the days from selected month and year', function() {               
        $rootScope.$apply();

        $scope.selected_year = 2017;
        $scope.selected_month = 4;

        $scope.GetDaysFromMonth();

        expect($scope.days.length).toBe(31);      
    });

    it('should update the Vacancy Component', function() {               
        var expected_total_days_int_unit_type = 3;

        $rootScope.$apply();

        $scope.UpdateVacancyComponent({
            unit_type_id: 2,
            days: [
                {
                    num: new Date(2017, 3, 2)
                },
                {
                    num: new Date(2017, 3, 3)
                }
            ]
        });

        expect($scope.units_type[1].days.length).toBe(expected_total_days_int_unit_type);      
    });
});