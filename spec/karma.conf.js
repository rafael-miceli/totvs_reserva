module.exports = function (config) {
  config.set({
    basePath: './',
    preprocessors: {
      '../src/js/**/*.js': ['coverage'],
      'fixtures/**/*.json': ['json_fixtures']
    },
    files: [
      "../src/lib/lodash/dist/lodash.min.js", 
      "../src/lib/angular/angular.min.js",
      "../src/lib/angular-mocks/angular-mocks.js",
      "../src/lib/angular-animate/angular-animate.js",
      "../src/lib/moment/min/moment.min.js",    
      "../src/lib/moment/min/locales.min.js",
      "../src/lib/moment/min/moment-with-locales.min.js",    
      "../src/lib/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min.js",

      '../src/js/**/*.js',
      'fixtures/**/*.json',
      'unit/**/*.js'
    ],
    autoWatch: true,
    frameworks: ['jasmine'],
    browsers: ['PhantomJS'],
    plugins: [
      'karma-phantomjs-launcher',
      'karma-coverage',
      'karma-jasmine',
      'karma-json-fixtures-preprocessor'
    ],
    jsonFixturesPreprocessor: {
      camelizeFilenames: true
    }
  });
};
